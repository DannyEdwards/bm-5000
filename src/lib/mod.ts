/** JavaScript's `%` operator doesn't work as expected with negative numbers. */
export default function mod(number: number, modulo: number) {
  return ((number % modulo) + modulo) % modulo;
}
