export default function getTracksState() {
  let trackState: boolean[][] = [];
  globalThis.tracks.forEach(({ inputs }, index) => {
    trackState.push([]);
    inputs.forEach(({ checked }) => {
      trackState[index].push(checked);
    });
  });
  return trackState;
}
