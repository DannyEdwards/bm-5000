import "./audio-track";
import { AudioTrack } from "./audio-track";
import "./bpm-slider";
import { BpmSlider } from "./bpm-slider";
import "./play-button";
import "./saved-beats";
import { SavedBeats } from "./saved-beats";

declare global {
  var activeBeatIndex: number;
  var audioCtx: AudioContext;
  var bpmSlider: BpmSlider;
  var savedBeats: SavedBeats;
  var tracks: NodeListOf<AudioTrack>;
}

globalThis.activeBeatIndex = 0;
globalThis.audioCtx = new AudioContext();
globalThis.bpmSlider = document.querySelector("bpm-slider");
globalThis.savedBeats = document.querySelector("saved-beats");
globalThis.tracks = document.querySelectorAll("audio-track");
