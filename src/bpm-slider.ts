import { LitElement, css, html } from "lit";
import { customElement, property, query } from "lit/decorators.js";

@customElement("bpm-slider")
export class BpmSlider extends LitElement {
  @property() min = 60;
  @property() max = 180;
  @property() value = 120;
  @query("input") input: HTMLInputElement;

  private handleInput() {
    this.value = parseInt(this.input.value, 10);
  }

  public get tempo() {
    return parseInt(this.input.value, 10);
  }

  public set tempo(value: number) {
    this.value = value;
    this.input.value = value.toString();
  }

  static styles = css`
    :host {
      font-weight: bold;
      flex-grow: 1;
      max-width: 30rem;
    }
    label {
      align-items: center;
      display: flex;
      gap: 0.5rem;
      height: 2rem;
    }
    input {
      -webkit-appearance: none;
      background: transparent;
      width: 100%;
    }
    input::-ms-track {
      width: 100%;
      cursor: pointer;
      background: transparent;
      border-color: transparent;
      color: transparent;
      height: 1rem;
      cursor: pointer;
      background-color: var(--black);
      border-radius: var(--border-radius);
    }
    input::-webkit-slider-runnable-track {
      height: 1rem;
      cursor: pointer;
      background-color: var(--black);
      border-radius: var(--border-radius);
    }
    input::-moz-range-track {
      height: 1rem;
      cursor: pointer;
      background-color: var(--black);
      border-radius: var(--border-radius);
    }
    input::-webkit-slider-thumb {
      -webkit-appearance: none;
      margin-top: -1rem;
      height: 3rem;
      width: 2rem;
      border: var(--border);
      border-radius: var(--border-radius);
      background-color: var(--lightgrey);
      cursor: pointer;
    }
    input:focus::-webkit-slider-thumb {
      border-style: double;
    }
    input::-moz-range-thumb {
      height: 2rem;
      width: 1rem;
      border: var(--border);
      border-radius: var(--border-radius);
      background-color: var(--lightgrey);
      cursor: pointer;
    }
    input:focus::-moz-range-thumb {
      border-style: double;
    }
    input::-ms-thumb {
      height: 2rem;
      width: 1rem;
      border: var(--border);
      border-radius: var(--border-radius);
      background-color: var(--lightgrey);
      cursor: pointer;
    }
    input:focus::-ms-thumb {
      border-style: double;
    }
    input:focus {
      outline: none;
    }
    input:active::-webkit-slider-thumb {
      background-color: var(--yellow);
    }
    input:active::-moz-range-thumb {
      background-color: var(--yellow);
    }
    input:active::-ms-thumb {
      background-color: var(--yellow);
    }
    output {
      width: 4rem;
    }
  `;

  render() {
    return html`
      <label>
        <slot>BPM</slot>
        <input
          type="range"
          min="${this.min}"
          max="${this.max}"
          .value="${this.value}"
          @input="${this.handleInput}"
        />
        <output>${this.value}</output>
      </label>
    `;
  }
}
