import { LitElement, css, html } from "lit";
import { customElement, property, queryAll } from "lit/decorators.js";

@customElement("audio-track")
export class AudioTrack extends LitElement {
  @property() audiosrc: string;
  @property({ attribute: false }) audioBuffer: AudioBuffer;
  @queryAll("input") inputs: NodeListOf<HTMLInputElement>;

  async connectedCallback() {
    super.connectedCallback();
    const response = await fetch(this.audiosrc);
    if (!response.ok) throw new Error(response.statusText);
    const arrayBuffer = await response.arrayBuffer();
    this.audioBuffer = await globalThis.audioCtx.decodeAudioData(arrayBuffer);
  }

  public play(time: number) {
    const sampleSource = new AudioBufferSourceNode(globalThis.audioCtx, {
      buffer: this.audioBuffer,
    });
    sampleSource.connect(globalThis.audioCtx.destination);
    sampleSource.start(time);
    return sampleSource;
  }

  static styles = css`
    :host {
      align-items: center;
      display: grid;
      gap: 1rem;
      grid-template-columns: 1fr 11fr;
    }
    section {
      display: flex;
      gap: 0.5rem;
      justify-content: space-between;
    }
    input {
      --background-color: var(--red);
      appearance: none;
      background-color: var(--background-color);
      border-radius: var(--border-radius);
      box-shadow: var(--box-shadow);
      border: var(--border);
      height: 5rem;
      margin: 0;
      text-align: center;
      width: 100%;
      z-index: 1;
    }
    input:focus {
      border-style: double;
      outline: none;
    }
    input:nth-of-type(n + 5) {
      --background-color: var(--orange);
    }
    input:nth-of-type(n + 9) {
      --background-color: var(--yellow);
    }
    input:nth-of-type(n + 13) {
      --background-color: var(--white);
    }
    input::before {
      content: "⚪️";
      display: block;
      margin-top: 0.25rem;
    }
    input:checked::before,
    input.active::before {
      content: "🔴";
    }
    input:active,
    input:checked.active {
      transform: translate(0.25rem, 0.25rem);
      box-shadow: var(--box-shadow-active);
      z-index: 0;
    }
    input:checked.active {
      animation: 0.2s shockwave;
    }
    @keyframes shockwave {
      from {
        box-shadow:
          var(--box-shadow-active),
          0 0 0 0 var(--background-color);
      }
      to {
        box-shadow:
          var(--box-shadow-active),
          0 0 0 4rem transparent;
      }
    }
    @media (max-width: 1080px) {
      :host {
        grid-template-columns: 1fr 7fr;
      }
      input:nth-of-type(2n) {
        display: none;
      }
    }
    @media (max-width: 512px) {
      :host {
        grid-template-columns: 1fr 5fr;
      }
      input:not(:nth-of-type(4n - 3)) {
        display: none;
      }
    }
  `;

  barTemplate(notes = 4) {
    return Array(notes).fill(html`<input type="checkbox" />`);
  }

  render() {
    return html`
      <slot></slot>
      <section>
        ${this.barTemplate()} ${this.barTemplate()} ${this.barTemplate()}
        ${this.barTemplate()}
      </section>
    `;
  }
}
