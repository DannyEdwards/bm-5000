import { LitElement, html } from "lit";
import { customElement } from "lit/decorators.js";
import getTracksState from "./lib/getTrackState";

@customElement("download-link")
export class DownloadLink extends LitElement {
  private handleClick({ currentTarget }) {
    const name = prompt("Name your beat.");
    if (!name) return;
    const tracksState = getTracksState();
    currentTarget.href = `data:text/json;charset=utf-8,${encodeURIComponent(
      JSON.stringify(tracksState),
    )}`;
    currentTarget.download = `${encodeURIComponent(name.replaceAll(" ", "_"))}.json`;
  }

  render() {
    return html`
      <a href="#" @click="${this.handleClick}">
        <slot>Download</slot>
      </a>
    `;
  }
}
