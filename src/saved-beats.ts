import { LitElement, css, html } from "lit";
import { customElement, queryAll, state } from "lit/decorators.js";
import { repeat } from "lit/directives/repeat.js";
import getTracksState from "./lib/getTrackState";

type Sequence = boolean[][];

const STORAGE_KEY = "beats";

@customElement("saved-beats")
export class SavedBeats extends LitElement {
  @state() beats: { name: string; sequence: Sequence; tempo: number }[] = [];
  @queryAll("input") inputs: NodeListOf<HTMLInputElement>;
  @queryAll("li") listItems: NodeListOf<HTMLLIElement>;

  constructor() {
    super();
    this.beats = JSON.parse(localStorage.getItem(STORAGE_KEY)) || [];
  }

  private handleSaveClick() {
    const name = prompt("Name your beat.");
    if (!name) return;
    this.beats = [
      ...this.beats,
      {
        name: encodeURIComponent(name),
        sequence: getTracksState(),
        tempo: globalThis.bpmSlider.tempo,
      },
    ];
    localStorage.setItem(STORAGE_KEY, JSON.stringify(this.beats));
  }

  public loadBeatIndex(index: number) {
    return () => {
      this.inputs.forEach((input) => input.classList.remove("active"));
      if (!this.inputs[index].checked) return;
      this.beats[index].sequence.forEach((track, trackIndex) => {
        track.forEach((isChecked, noteIndex) => {
          globalThis.tracks[trackIndex].inputs[noteIndex].checked = isChecked;
        });
      });
      globalThis.bpmSlider.tempo = this.beats[index].tempo;
      globalThis.activeBeatIndex = index;
      this.inputs[index].classList.add("active");
    };
  }

  protected loadBeat(sequence: Sequence, tempo = 120) {
    return () => {
      sequence.forEach((track, trackIndex) => {
        track.forEach((isChecked, noteIndex) => {
          globalThis.tracks[trackIndex].inputs[noteIndex].checked = isChecked;
        });
      });
      globalThis.bpmSlider.tempo = tempo;
    };
  }

  private handleDeleteClick(id: number, name: string) {
    return () => {
      if (!confirm(`Delete "${decodeURIComponent(name)}"?`)) return;
      this.beats = this.beats.filter((_, index) => id !== index);
      localStorage.setItem(STORAGE_KEY, JSON.stringify(this.beats));
    };
  }

  private handleDragStart(event: DragEvent) {
    if (!(event.target instanceof HTMLLIElement)) return;
    event.dataTransfer.effectAllowed = "move";
    event.dataTransfer.setData("text/plain", event.target.id);
  }

  private handleDragOver(event: DragEvent) {
    event.preventDefault(); // @note This is needed for drop to fire.
    if (!(event.target instanceof HTMLElement)) return;
    if (event.target.parentElement instanceof HTMLLIElement)
      event.target.parentElement.classList.add("over");
  }

  private handleDragLeave(event: DragEvent) {
    if (!(event.target instanceof HTMLElement)) return;
    if (event.target.parentElement instanceof HTMLLIElement)
      event.target.parentElement.classList.remove("over");
  }

  private handleDrop(event: DragEvent) {
    if (!(event.target instanceof HTMLElement)) return;
    event.dataTransfer.dropEffect = "move";
    const draggedId = event.dataTransfer.getData("text/plain");
    const swapId = event.target.parentElement.id;
    if (!draggedId || !swapId) return;
    [this.beats[swapId], this.beats[draggedId]] = [
      this.beats[draggedId],
      this.beats[swapId],
    ];
    this.requestUpdate();
    localStorage.setItem(STORAGE_KEY, JSON.stringify(this.beats));
  }

  private handleDragEnd() {
    this.listItems.forEach(({ classList }) => classList.remove("over"));
  }

  static styles = css`
    ul {
      align-items: center;
      display: flex;
      flex-wrap: wrap;
      gap: 1rem;
      list-style: none;
      margin: 0;
      padding: 0;
    }
    li {
      display: flex;
    }
    button,
    label {
      --box-shadow: 0.125rem 0.125rem 0 var(--box-shadow-colour);
      background-color: var(--lightgrey);
      border: 0.25rem solid var(--black);
      border-radius: var(--border-radius);
      box-shadow: var(--box-shadow);
      color: var(--black);
      cursor: grab;
      font-family: Futura, sans-serif;
      font-size: 1rem;
      padding-left: 0.5rem;
      padding-right: 0.5rem;
    }
    button:active,
    label:active,
    label:has(input.active) {
      box-shadow: var(--box-shadow-active);
      transform: translate(0.125rem, 0.125rem);
    }
    label:active {
      cursor: grabbing;
    }
    label {
      border-top-right-radius: 0;
      border-bottom-right-radius: 0;
    }
    label:has(input:checked) {
      background-color: var(--yellow);
    }
    label:has(input:focus) {
      border-style: double;
    }
    label::before {
      content: "⚪️ ";
    }
    label:has(input.active)::before {
      content: "🔴 ";
    }
    input {
      appearance: none;
      margin: 0;
      width: 0;
    }
    input:focus,
    button:focus {
      outline: none;
    }
    button {
      cursor: pointer;
    }
    button[title="Delete"] {
      border-left: 0;
      border-top-left-radius: 0;
      border-bottom-left-radius: 0;
      background-color: var(--grey);
    }
    button.save {
      background-color: var(--green);
    }
    button:focus {
      border-style: double;
    }
    .over {
      opacity: 0.5;
    }
    .over button,
    .over label {
      border-style: dotted;
    }
  `;

  render() {
    return html`
      <ul>
        ${repeat(
          this.beats,
          ({ name }) => name,
          ({ name }, index) => html`
            <li
              id="${index}"
              draggable="true"
              @dragstart="${this.handleDragStart}"
              @dragover="${this.handleDragOver}"
              @dragleave="${this.handleDragLeave}"
              @drop="${this.handleDrop}"
              @dragend="${this.handleDragEnd}"
            >
              <label
                ><input
                  type="checkbox"
                  name="beats"
                  @change="${this.loadBeatIndex(index)}"
                  title="Play ${decodeURIComponent(name)}"
                />${decodeURIComponent(name)}</label
              >
              <button
                @click="${this.handleDeleteClick(index, name)}"
                title="Delete"
              >
                X
              </button>
            </li>
          `,
        )}
        <li>
          <button @click="${this.handleSaveClick}" class="save">
            Save beat
          </button>
        </li>
      </ul>
    `;
  }
}
