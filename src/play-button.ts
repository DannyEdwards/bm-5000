import { LitElement, css, html } from "lit";
import { customElement, state } from "lit/decorators.js";
import mod from "./lib/mod";

@customElement("play-button")
export class PlayButton extends LitElement {
  @state() bars = 4;
  @state() currentIndex = 0;
  @state() nextNoteTime = 0.0;

  private handleChange({ target }) {
    if (target.checked) {
      globalThis.audioCtx.resume();
      this.scheduler();
    } else {
      globalThis.audioCtx.suspend();
    }
  }

  private scheduler() {
    const previousIndex = mod(this.currentIndex - 1, this.bars * 4);
    while (globalThis.audioCtx.currentTime + 0.1 > this.nextNoteTime) {
      globalThis.tracks.forEach((track) => {
        const currentInput = track.inputs[this.currentIndex];
        if (currentInput.checked && currentInput.checkVisibility())
          track.play(this.nextNoteTime);
        currentInput.classList.add("active");
        track.inputs[previousIndex].classList.remove("active");
      });
      this.currentIndex = (this.currentIndex + 1) % (this.bars * 4);
      this.nextNoteTime += 60.0 / globalThis.bpmSlider.tempo / this.bars;
      if (this.currentIndex === 0) this.savedBeatsScheduler();
    }
    requestAnimationFrame(() => this.scheduler());
  }

  private savedBeatsScheduler() {
    const { savedBeats, activeBeatIndex } = globalThis;
    if (!savedBeats.inputs.length) return;
    let currentIndex = (activeBeatIndex + 1) % savedBeats.inputs.length;
    do {
      if (savedBeats.inputs[currentIndex]?.checked)
        return savedBeats.loadBeatIndex(currentIndex)();
      currentIndex = (currentIndex + 1) % savedBeats.inputs.length;
    } while (currentIndex !== activeBeatIndex);
  }

  static styles = css`
    label {
      font-size: 2rem;
      text-align: center;
      background-color: var(--yellow);
      display: block;
      padding: 0.25rem;
      border: var(--border);
      border-radius: var(--border-radius);
      box-shadow: var(--box-shadow);
      cursor: pointer;
      width: 6rem;
    }
    input {
      appearance: none;
      margin: 0;
      width: 0;
    }
    input:focus {
      outline: none;
    }
    label:has(input:checked) {
      box-shadow: var(--box-shadow-active);
      transform: translate(0.25rem, 0.25rem);
    }
    label:has(input:focus) {
      border-style: double;
    }
    slot[name="pause-label"],
    label:has(input:checked) slot[name="play-label"] {
      display: none;
    }
    slot[name="play-label"],
    label:has(input:checked) slot[name="pause-label"] {
      display: inline;
    }
  `;

  render() {
    return html`<label
      ><slot name="play-label">Play</slot><slot name="pause-label">Pause</slot
      ><input type="checkbox" @change="${this.handleChange}"
    /></label>`;
  }
}
